import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from "./employee";
import { Registration } from "./registration";
@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  constructor(private http:HttpClient) { }

  private baseUrl="http://localhost:9090/employees";
login(username:string,password:string){
  const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
  return this.http.get("http://localhost:9090/",{headers,responseType: 'text' as 'json'})
}
getEmployeeList():Observable<Employee[]>{
  return this.http.get<Employee[]>("http://localhost:9090/employees");
}
createEmployee(employee: Employee): Observable<any>{
  return this.http.post("http://localhost:9090/employees/add",employee);
}

getEmployeeByid(id: number):Observable<any>{
  return this.http.get<Employee>(`${this.baseUrl}/${id}`);
}
updateEmployee(id: number, employee: Employee): Observable<Object>{
  return this.http.put(`${this.baseUrl}/${id}`, employee);
}
deleteEmployee(id: number): Observable<Object>{
  return this.http.delete(`${this.baseUrl}/${id}`);
}
registerUser(register: Registration):Observable<any>{
   return this.http.post("http://localhost:9090/admin/add",register);
}
}
